Graphical robots
================

.. inheritance-diagram:: grobots

.. automodule:: grobots
    :members:
