#!/bin/sh

# Script to create translation file.

pygettext.py -d base -o base.pot ../src/tobject.py ../src/trobots.py\
    ../src/tworld.py ../hformatter ../src/tmove.py ../gworld.py ../gmove.py

# Then, you need to create a $language/LC_MESSAGES/ sub-folder , where
# $language is the code for the language you target. Copy “base.pot” in this
# sub-folder, rename it “base.po”, fill translations in and execute the
# following command in it:
#     msgfmt.py -o base.mo base
