Robots on the move
==================

This is a pedagogical project which aims to present the pythonic way for
coding, especially how to make class structure and modules. It also shows
how to create a client-server system.

In this project, we define a rectangular world composed with several squares.
In this world, some robots move. The robots do not know what the geometry of
the world is, nor where are the other robots: they ask for some movement to
the world, which makes the robots move if this move is possible. A robot can
only move from one square to another and only in four direction: north,
south, east, and west. There are several kinds of robots: fixed robots which
do not move, crazy robots which move randomly, obstinate robots which move
the same direction as long as it is possible, and interactive robots which
receive moving orders from the outside.

This is the Python version of a pedagogical project coming from the following
book:

Chailloux, Emmanuel, Pascal Manoury, and Bruno Pagano, 2000. _Développement
d’applications avec Objective Caml_, O’Reilly.
[Available on-line](https://www-apr.lip6.fr/~chaillou/Public/DA-OCAML/ "Développement
d’applications avec Objective Caml").
An English version is
[available on-line](https://caml.inria.fr/pub/docs/oreilly-book/ "Developing applications with Objective Caml").

[Code documentation is available on-line.](https://ylebars.frama.io/robots-on-the-move-python-version/ "Robot on the move code documentation")

**Contents**

* [Running](#running "Runnin")
* [Generate documentation](#generate-documentation "Generate documentation")
* [Licence](#licence "Licence")

Running
-------

This code can be run on any system which provides
[Python](https://www.python.org/ "Python scripting language") version 3.10 or
greater. It uses the following modules:

* [Python gettext](https://github.com/hannosch/python-gettext "Python gettext library")
    version 4.1 or greater;
* [Multiplemethod](https://github.com/coady/multimethod "Multiplemethod");
* [Pygame](https://www.pygame.org/ "Pygame") 2.0 or greater.

Two versions are available: text version and graphical version. To run the
text version, simply run the script named “`tmove.py`” in the folder named
“`src`”. Here is the command line usage:

```bash
tmove.py [-h] [-v]
```

Option `-h` (or `--help`) displays a help message and terminate the program,
while option `-v` (or `--version`) displays the program version and terminate
the program.

To run the graphical version, simply run the script named “`gmove.py`” in the
folder named “`src`”. Program options are the same as the previous script.
To leave this script, either hit the `Q` key, the `Esc` key, or click on the cross icon on the
application window. To move the interactive robot, use the arrow keys.

Generate documentation
----------------------

Documentation of this code is generate using
[Sphinx](https://www.sphinx-doc.org/ "Sphinx documentation system") through
[Graphviz](https://graphviz.org/ "Graphviz") and uses the theme
[Read the Docs](https://readthedocs.org/ "Read the Docs")
(`sphinx-rtd-theme`). To generate it, go to the `doc` folder. Running the
`make` command, you will get the list of available documentation formats. For
instance, to generate documentation in HTML format, run the following
command:

```bash
make html
```

A sub folder will then be generated in “`doc/build`”. In the previous
example, the documentation will be generated in the folder
“`doc/build/html`”, the main page being “`index.html`”.

Licence
-------

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence as published by the
Free Software Foundation; either version 3 of the licence, or (at your
option) any later version. See the file named
“[`LICENSE`](LICENSE "LICENSE")” or
[on-line](https://www.gnu.org/licenses/gpl-3.0.html "GNU GPL 3.0").

Background image by [Rawpixel.com](https://www.rawpixel.com/ "Rawpixel.com")
through [Freepik.com.](https://www.freepik.com/ "Freepik.com").

Fixed robot image under licence
[CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/ "CC-BY-SA 3.0")
by [Phaelax](https://opengameart.org/users/phaelax "Phaelax") through
[OpenGameArt.Org](https://opengameart.org/ "OpenGameArt.Org").

Obstinate robot image by
[Open Clipart](https://openclipart.org/ "Open Clipart").

Crazy robot image by
[Warspawn](https://opengameart.org/users/warspawn "Warspawn") through
[OpenGameArt.Org](https://opengameart.org/ "OpenGameArt.Org").

Interactive robot image by [Creazilla](https://creazilla.com/ "Creazilla.com").

Copyright © 2023 [Yoann LE BARS](http://le-bars.net/yoann/ "A View from here").
