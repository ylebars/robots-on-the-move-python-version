#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definitions for robots handling.

:module: robots
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from typing import Tuple, TypeVar, Type, Final
from enum import Enum, unique
import abc
import random
from errors import InvalidDirection


#: Type defining a robot position.
Position = Tuple[int, int]
#: Create a generic variable that can be 'Robot', or any subclass.
T = TypeVar('T', bound='Robot')


@unique
class Direction(Enum):
    """
    Enumeration of possible directions for moves.
    """

    NORTH: Final[int] = 0
    SOUTH: Final[int] = 1
    EAST: Final[int] = 2
    WEST: Final[int] = 3
    NOTHING: Final[int] = 4


class Robot(metaclass=abc.ABCMeta):
    """
    Class defining a generalist robot.

    :param Position __position: Robot current position.
    """

    __position: Position

    @property
    def position(self) -> Position:
        """
        Gives current position of the robot.

        :returns: A tuple containing current coordinates.
        :rtype: Position
        """

        return self.__position

    @position.setter
    def position(self, position: Position) -> None:
        """
        Set position of the robot.

        :param Position position: New coordinates.
        """

        self.__position = position

    def __init__(self, position: Position = (0, 0)) -> None:
        """
        Class initialization.

        :param Position position: Coordinates for robot initial position.
                                  Defaults to (O, O).
        """

        self.position = position

    @classmethod
    def from_int(cls: Type[T], abscissa: int, ordinate: int) -> T:
        """
        Class initialization using two integer.

        :param int abscissa: Robot starting point abscissa.
        :param int ordinate: Robot starting point ordinate.

        :returns: An instance of the class.
        :rtype: T
        """

        return cls((abscissa, ordinate))

    @abc.abstractmethod
    def next_position(self) -> Position:
        """
        Determine in which position the robot will go.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

    def _walk(self, direction: Direction) -> Position:
        """
        Make the robot move in a given direction.

        :param Direction direction: Indicates in which direction the robot
                                    will move.

        :returns: Position to go coordinates.
        :rtype: Position

        :raises InvalidDirection: If an invalid direction is asked for.
        """

        match direction:
            case Direction.NORTH:
                return self.__position[0], self.__position[1] + 1
            case Direction.SOUTH:
                return self.__position[0], self.__position[1] - 1
            case Direction.EAST:
                return self.__position[0] + 1, self.__position[1]
            case Direction.WEST:
                return self.__position[0] - 1, self.__position[1]
            case Direction.NOTHING:
                return self.position
            case _:
                raise InvalidDirection()

    @staticmethod
    def _turn_right(direction: Direction) -> Direction:
        """
        Indicates the new direction when the robot has turned right.

        :param Direction direction: Initial direction.
        :returns: New direction.
        :rtype: Direction

        :raises InvalidDirection: If an invalid direction is asked for.
        """

        match direction:
            case Direction.NORTH:
                return Direction.EAST
            case Direction.EAST:
                return Direction.SOUTH
            case Direction.SOUTH:
                return Direction.WEST
            case Direction.WEST:
                return Direction.NORTH
            case Direction.NOTHING:
                return Direction.NOTHING
            case _:
                raise InvalidDirection()


class FixedRobot(Robot):
    """
    Class for not moving robot.
    """

    def next_position(self) -> Position:
        """
        Determine in which position the robot will go.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

        return self.position


class CrazyRobot(Robot):
    """
    Class defining a robot which moves randomly.
    """

    def next_position(self) -> Position:
        """
        Determine in which position the robot will go.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

        return self._walk(random.choices([Direction.NORTH, Direction.SOUTH,
                                          Direction.EAST,
                                          Direction.WEST])[0])


#: Create a generic variable that can be 'ObstinateRobot', or any subclass.
U = TypeVar('U', bound='ObstinateRobot')


class ObstinateRobot(Robot):
    """
    Class defining a robot which always moves in the same direction as long
    as it is possible.

    :param tuple __wanted_position: Next position in which the robot wants to
                                    go.
    :param Direction __direction: Direction in which the robot wants to go.
    """

    __wanted_position: Position
    __direction: Direction

    def __init__(self, position: Position = (0, 0)) -> None:
        """
        Class initialization.

        :param Position position: Coordinates for the robot initial position.
                                  Defaults to (0 ; 0).
        """

        Robot.__init__(self, position)
        self.__wanted_position = position
        self.__direction = Direction.WEST

    @property
    def direction(self) -> Direction:
        """
        Gives current position of the robot.

        :returns: Current position of the robots in the world.
        :rtype: Direction
        """

        return self.__direction

    @classmethod
    def from_int(cls: Type[U], abscissa: int, ordinate: int) -> U:
        """
        Class initialization using two integer.

        :param int abscissa: Robot starting point abscissa.
        :param int ordinate: Robot starting point ordinate.

        :returns: An instance of the class.
        :rtype: U
        """

        return cls((abscissa, ordinate))

    def __change_direction(self) -> None:
        """
        Makes the robot change direction.
        """

        self.__direction = self._turn_right(self.__direction)

    def next_position(self) -> Position:
        """
        Determine in which position the robot will go.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

        if self.position == self.__wanted_position:
            self.__wanted_position = self._walk(self.__direction)
        else:
            self.__change_direction()
            self.__wanted_position = self.position

        return self.__wanted_position


class InteractiveRobot(Robot, metaclass=abc.ABCMeta):
    """
    Robot which receive moving orders from the outside.
    """

    @abc.abstractmethod
    def _get_move(self) -> Direction:
        """
        Method allowing the robot to receive some moving order.

        :returns: The direction in which the robot should go.
        :rtype: Direction
        """

    def next_position(self) -> Position:
        """
        Determine in which position the robot will go.

        :returns: Wanted position coordinates.
        :rtype: Position
        """

        return self._walk(self._get_move())
