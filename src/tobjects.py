#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definitions of textual objects.

:module: tobjects
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from translator import _


class TextualObject:    # pylint: disable=too-few-public-methods
    """
    Class defining a text object.

    :param str __object_name: Name of the object.
    """

    __object_name: str

    def __init__(self, object_name: str = _('Anonymous object')) -> None:
        """
        Object initialization.

        :param str object_name: Name of the object. Defaults to
                                “Anonymous object”.
        """

        self.__object_name = object_name

    @property
    def object_name(self) -> str:
        """
        Gives object name.

        :returns: The name of the object.
        :rtype: str
        """

        return self.__object_name
