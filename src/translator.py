#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Utility function for translation.

:module: translator
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from typing import Final
import os
import gettext
import locale
from copy import copy
from inspect import currentframe


# Path to the localization directory.
LOCALEDIR: Final[str] = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../locales/')
locale.setlocale(locale.LC_ALL, '')
# Current system locale setup.
CURRENTLOCALE: Final[tuple] = locale.getlocale(locale.LC_MESSAGES)
# Translator.
translator = gettext.translation('base', LOCALEDIR, languages=CURRENTLOCALE)
translator.install()
#: Translator shortcut.
_ = translator.gettext


def f(string: str) -> str:
    """
    Function to use f-strings with gettext.

    :param str string: The formatted string to translate.

    :return: The string in a gettext compatible format.
    """

    # Frame of this function.
    frame = currentframe().f_back
    # Construct the new arguments.
    kwargs = copy(frame.f_globals)
    kwargs.update(frame.f_locals)
    return string.format(**kwargs)
