#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definitions of graphical objects.

:module: gobjects
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from typing import Final, Any, AnyStr
# Getting rid of Pygame greetings prompt.
from os import environ
environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'
import pygame    # pylint: disable=wrong-import-position


#: Sprite size
SPRITESIZE: Final[int] = 30


class GraphicalObject(pygame.sprite.Sprite):
    """
    Class defining a graphical object.

    :param pygame.Surface __image: Sprite image descriptor.
    :param Any _rect: Descriptor for the rectangle containing the sprite.
    """

    __image: pygame.Surface
    _rect: Any

    def __init__(self, file_path: AnyStr) -> None:
        """
        Class initialization.

        :arg AnyStr file_name: Path to the file containing the image to draw.
        """

        pygame.sprite.Sprite.__init__(self)

        self.__image = pygame.image.load(file_path).convert_alpha()
        self.__image = pygame.transform.scale(self.__image,
                                              (SPRITESIZE, SPRITESIZE))
        self._rect = self.__image.get_rect()

    @property
    def image(self) -> pygame.Surface:
        """
        Returns a descriptor to the image of the sprite.

        :returns: Image descriptor.
        :rtype: pygame.Surface
        """

        return self.__image

    @property
    def rect(self) -> Any:
        """
        Returns a descriptor of the rectangle containing the sprite.

        :returns: Rectangle descriptor.
        :rtype: Any
        """

        return self._rect

    def change_position(self, abscissa: int, ordinate: int) -> None:
        """
        Modifying sprite position.

        :param int abscissa: New abscissa.
        :param int ordinate: New ordinate.
        """

        self._rect.center = (abscissa, ordinate)

    def update(self, *args: Any, **kwargs: Any) -> None:
        """
        What is to be done when updating the sprite.
        """

        return pygame.sprite.Sprite.update(self, *args, **kwargs)
