#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Classes for errors handling.

:module: errors
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


class Error(Exception):
    """
    Base class for exceptions handling
    """


class InvalidDirection(Error):
    """
    Exception raised when asked for some invalid direction.
    """
