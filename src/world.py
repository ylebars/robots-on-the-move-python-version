#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definitions for world handling.

:module: world
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from typing import TypeVar, Generic
import abc
from robots import Position


#: Type for an object in the world.
WorldObject = TypeVar('WorldObject')


class World(Generic[WorldObject], metaclass=abc.ABCMeta):
    """
    Class defining a world in which robots will move.

    :param int __length: World length, in number of squares.
    :param int __height: World height, in number of squares.
    :param list[WorldObject] __objects: List of objects in the world.
    """

    __length: int
    __height: int
    _objects: list[WorldObject]

    def __init__(self, length: int = 0, height: int = 0) -> None:
        """
        Class initialization.

        :param int length: Initial length. Defaults to 0.
        :param int height: Initial height. Defaults to 0.
        """

        self.__length = length
        self.__height = height
        self._objects = []

    @property
    def length(self) -> int:
        """
        Gives the length of the world.

        :returns: Length of the world, in of squares.
        :rtype: int
        """

        return self.__length

    @property
    def height(self) -> int:
        """
        Gives the height of the world.

        :returns: Height of the world, in of squares.
        :rtype: int
        """

        return self.__height

    @property
    def objects(self) -> list[WorldObject]:
        """
        Gives the list of objects in the world.

        :returns: The list og objects.
        :rtype: list[WorldObject]
        """

        return self._objects

    def add_object(self, new_object: WorldObject) -> None:
        """
        Add a robot in the world.

        :param WorldObject new_object: The robot to be added.
        """

        self._objects.append(new_object)

    def is_free(self, position: Position) -> bool:
        """
        Indicates if a position is available.

        :param Position position: Position to be tested.

        :returns: Whether the position is available.
        :rtype: bool
        """

        return position not in [current.position for current in self._objects]

    @abc.abstractmethod
    def is_legal(self, position: Position) -> bool:
        """
        Indicates if a position is valid in the given world.

        :param Position position: Position to be tested.

        :returns: Whether the position is available.
        :rtype: bool
        """

    @abc.abstractmethod
    def _move_object(self, current_object: WorldObject) -> None:
        """
        Moves a given object.

        :param WorldObject current_object: The object to be moved.
        """

        # Asked next position.
        position: Position = current_object.next_position()
        if self.is_legal(position) and self.is_free(position):
            current_object.position = position

    @abc.abstractmethod
    def _determine_continue_running(self) -> bool:
        """
        Determine if the program should continue running.

        :returns: Whether continue.
        :rtype: bool
        """

    def run(self) -> None:
        """
        Program main loop.
        """

        while self._determine_continue_running():
            for current_object in self._objects:
                self._move_object(current_object)
