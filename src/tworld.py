#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Definitions of textual world.

:module: tworld
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


from world import World
from trobots import TextualRobot
from robots import Position
from translator import _, f


# List of positive answers.
positive_answers: list[str] = [_('y'), _('Y'), _('yes'), _('Yes'), _('YES')]
# List of negative answers.
negative_answers: list[str] = [_('n'), _('N'), _('no'), _('No'), _('NO')]


class TextualWorld (World[TextualRobot]):
    """
    Defining a textual world.

    :param bool __first_step: Whether it is the first step in run.
    """

    __first_step: bool

    def __init__(self, length: int = 0, height: int = 0) -> None:
        """
        Class initialization.

        :param int length: Initial length. Defaults to 0.
        :param int height: Initial height. Defaults to 0.
        """

        World.__init__(self, length, height)
        self.__first_step = True

    def is_legal(self, position: Position) -> bool:
        """
        Indicates if a position is valid in the given world.

        :param Position position: Position to be tested.

        :returns: Whether the position is available.
        :rtype: bool
        """

        return 0 <= position[0] < self.length\
            and 0 <= position[1] < self.height

    def _move_object(self, current_object: TextualRobot) -> None:
        """
        Moves a given robot.

        :param Robot current_object: The robot to be moved.
        """

        # Asked next position.
        position: Position = current_object.next_position()
        if self.is_legal(position) and self.is_free(position):
            print(f(_('{current_object.object_name} is moving from '
                      '{current_object.position} to {position}.')))
            current_object.position = position
        else:
            print(f(_('{current_object.object_name} is staying at '
                      '{current_object.position}.')))

    def _determine_continue_running(self) -> bool:
        """
        Determine if the program should continue running.

        :returns: Whether continue.
        :rtype: bool
        """

        if self.__first_step:
            self.__first_step = False
            return True

        while True:
            # User response
            response = input(_('Continue (yes or no)? '))
            if response in positive_answers:
                print()
                return True
            if response in negative_answers:
                return False
            print(_('Invalid input.'))

    def run(self) -> None:
        """
        Program main loop.
        """

        print(_('Initial state:'))
        for robot in self.objects:    # pylint: disable=unused-variable
            print(f(_('{robot.object_name} is at {robot.position}.')))
        print(_('\nRunning:'))
        World.run(self)
