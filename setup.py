#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Setup for the project Robots on the move, Python version.

:module: translator
:author: Le Bars, Yoann

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public Licence  as published by the
Free Software Foundation; either version 3 of the Licence, or (at your
option) any later version. See file `LICENSE` or go to:

https://www.gnu.org/licenses/gpl-3.0.html
"""


import sys
import os
from setuptools import setup
sys.path.insert(0, os.path.abspath('src/'))
from constants import __version__    # pylint: disable=wrong-import-position


setup(name = 'Robots on the move',
      version = __version__,
      description = 'Simple program making robots move in a minimalist '
                    'world.',
      author = 'Le Bars, Yoann',
      install_requires=['python-gettext >= 4.1', 'pygame', 'multimethod'],
      license = 'GNU GPL v3'
)
